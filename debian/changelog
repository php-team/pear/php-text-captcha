php-text-captcha (1.0.2-9) unstable; urgency=medium

  * Apply patches by Utkarsh Gupta used in Ubuntu as
    php-text-captcha_1.0.2-8ubuntu1 upload (Closes: #977403).
  * Override extended-description-contains-empty-paragraph lintian warning.
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Andrius Merkys <merkys@debian.org>  Thu, 14 Apr 2022 13:13:33 -0400

php-text-captcha (1.0.2-8) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Use secure URI in Homepage field.

  [ David Prévot ]
  * Handle patches with gbp pq
  * Adapt to recent version of PHPUnit (9)

 -- David Prévot <taffit@debian.org>  Mon, 14 Dec 2020 15:47:43 -0400

php-text-captcha (1.0.2-7) unstable; urgency=medium

  * Removing 'Words' driver as it depends on php-numbers-words, which will
    not make into Debian due to PHP license.

 -- Andrius Merkys <merkys@debian.org>  Fri, 27 Mar 2020 03:18:59 -0400

php-text-captcha (1.0.2-6) unstable; urgency=medium

  * Turning off test result caching for phpunit.
  * Bumping Standards-Version (no changes).

 -- Andrius Merkys <merkys@debian.org>  Mon, 17 Feb 2020 08:18:03 -0500

php-text-captcha (1.0.2-5) unstable; urgency=medium

  * Fixing FTBFS with phpunit (Closes: #882914)
  * Pointing Vcs-* tags to salsa.d.o
  * Making global files wildcard the first paragraph in debian/copyright
  * Adding myself to the list of the uploaders.
  * Adding 'Rules-Requires-Root: no'.
  * Using HTTPS for the format URI of the machine-readable copyright file.
  * Bumping debhelper compat level to 12 (no changes).
  * Converting all tabs to spaces in debian/copyright.

 -- Andrius Merkys <merkys@debian.org>  Thu, 13 Feb 2020 01:52:30 -0500

php-text-captcha (1.0.2-4) unstable; urgency=medium

  * Team upload

  [ Nishanth Aravamudan ]
  * Add missing dependency on php-pear (Closes: #827409)

  [ Prach Pongpanich ]
  * Update Standards-Version to 3.9.8

 -- Prach Pongpanich <prach@debian.org>  Thu, 16 Jun 2016 12:00:45 +0700

php-text-captcha (1.0.2-3) unstable; urgency=medium

  * Team upload
  * Use standard gbp layout
  * Use https in Vcs-* fields
  * PHP 7.0 transition:
    - Use php-gd instead of php5-gd
    - Rebuild with recent pkg-php-tools
  * Update Standards-Version to 3.9.7

 -- Prach Pongpanich <prach@debian.org>  Sat, 19 Mar 2016 22:05:12 +0700

php-text-captcha (1.0.2-2) unstable; urgency=medium

  * Team upload
  * Add missing test dependency
  * Correct Vcs-* URLs to point to anonscm.debian.org

 -- Prach Pongpanich <prach@debian.org>  Sun, 22 Nov 2015 22:15:21 +0700

php-text-captcha (1.0.2-1) unstable; urgency=medium

  * Team upload
  * Fix invalid homepage
  * Imported Upstream version 1.0.2
  * Bump debhelper compat to 9
  * Convert copyright to format 1.0
  * Use phppear substvars
  * Run test during package build
  * Add DEP-8 tests
  * Bump Standards-Version to 3.9.6

 -- Prach Pongpanich <prach@debian.org>  Sat, 14 Nov 2015 14:44:48 +0700

php-text-captcha (0.4.3-1) unstable; urgency=low

  * New upstream release.
  * Added a new debian/gbp.conf.
  * Switched debian/copyright to DEP5 format.
  * Now using 3.0 (quilt) source format.
  * Switched to pkg-php-tools and dh 8 sequencer.
  * Fixed Vcs fields (Closes: #638528).
  * Using Maintainer: PKG-PHP-PEAR team as maintainer.
  * Standards-Version: is now 3.9.2 (no change).
  * Fixed short description (remove article).

 -- Thomas Goirand <zigo@debian.org>  Fri, 03 Feb 2012 19:31:32 +0800

php-text-captcha (0.4.0-2) unstable; urgency=low

  * Updated debian/copyright to add the license for
    Text_CAPTCHA-0.4.0/CAPTCHA/Driver/Numeral.php

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Jul 2010 18:14:25 +0800

php-text-captcha (0.4.0-1) unstable; urgency=low

  * Initial release (Closes: #562747)

 -- Thomas Goirand <zigo@debian.org>  Mon, 05 Jul 2010 20:36:52 +0800
